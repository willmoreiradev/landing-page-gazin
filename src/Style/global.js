import styled from 'styled-components';



export const Container = styled.div`
  position: relative;
  width: 1200px;
  margin: 0 auto;
  padding: 0px 15px;
  @media(max-width: 1100px) {
    width: 100%;
  }
`;

