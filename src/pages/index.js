import React, { Component } from 'react'
import {Helmet} from "react-helmet";
import AOS from 'aos';

import ComponentHeader from '../components/header';
import Slide from '../components/slide-lp';
import Cadastro from '../components/section-cadastro';
import Confianca from '../components/section-confianca';
import Duvidas from '../components/section-duvidas';
import Tempo from '../components/section-tempo';
import Footer from '../components/footer';

import '../Style/main.css';

import 'aos/dist/aos.css';

class SiteMain extends Component {
  constructor(props){
    super(props);
  }
  componentDidMount(){
    AOS.init({
      once: true,
      disable: 'mobile',
      duration : 1000
    })
  }
  render() { 
    return ( 
      <div className="main">
        <Helmet>
            <meta charSet="utf-8" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <title>Landing Page Gazin</title>
            <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap" rel="stylesheet"></link>
        </Helmet>
        <ComponentHeader/>
        <Slide/>
        <Cadastro/>
        <Confianca/>
        <Duvidas/>
        <Tempo/>
        <Footer/>
      </div>
    );
  }
}
 
export default SiteMain;
