import React, { Component } from 'react'
import {Helmet} from "react-helmet";
import AOS from 'aos';

import HeaderCadastro from '../components/cadastro/header';
import AreaCadastro from '../components/cadastro/area-cadastro';
import FooterCadastro from '../components/cadastro/footer';

import '../Style/main.css';

import 'aos/dist/aos.css';

class SiteMain extends Component {
  constructor(props){
    super(props);
  }
  componentDidMount(){
    AOS.init({
      once: true,
      disable: 'mobile',
      duration : 1000
    })
  }
  render() { 
    return ( 
      <div className="main">
        <Helmet>
            <meta charSet="utf-8" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <title>Cadastro - Landing Page Gazin</title>
            <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap" rel="stylesheet"></link>
        </Helmet>
        <HeaderCadastro/>
        <AreaCadastro/>
        <FooterCadastro/>
      </div>
    );
  }
}
 
export default SiteMain;
