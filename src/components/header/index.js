import React, { Component } from 'react';

import { Link } from 'gatsby';

import { Header } from './style';

import { Container } from '../../Style/global';

//Images

import Logo from '../../images/logo.svg';


class ComponentHeader extends Component {
    state = {
        fixed: false,
        menu: false
    }
    componentWillMount = () => {
        if(typeof window != 'undefined') {
            window.addEventListener('scroll', () => {
                if(window.scrollY > 30) {
                    this.setState({ fixed : true })
                } else {
                    this.setState({ fixed : false })
                }
            })
        }
    }
    clickSection = (section) => {
        const tabScroll = document.getElementById(section);
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': tabScroll.offsetTop - 80
        });   
    }
    render() { 
        return ( 
            <Header menu_fixed={this.state.fixed} menu_opened={this.state.menu}>
                <Container className="container">
                    <div className="esq">
                        <Link to="/" className="logo">
                            <img src={Logo} alt="Logo Gazin" title="Logo Gazin"/>    
                        </Link>
                        <nav>
                            <ul>
                                <li>
                                <a href="" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('home')
                                }}>Home</a>
                                </li>
                                <li><a href="" target="_blank">Marketplace</a></li>
                                <li><a href="" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('cadastro')
                                }}>Como Funciona</a></li>
                                <li><a href="" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('duvidas')
                                }}>Dúvidas</a></li>
                            </ul>
                        </nav>
                    </div>
                    <a className="menu-button" onClick={(e) => {
                        e.preventDefault();
                        this.setState({ menu : !this.state.menu })
                    }}>
                        <i className="menu-icon"></i>
                    </a>
                    <div className="dir">
                        <a href="" className="btn login">Login</a>
                        <Link to="/cadastro" className="btn cadastro">Cadastre-se</Link>
                    </div>
                </Container>
                <div className="menu-mobile">
                    <ul>
                        <li><a href="" onClick={(e) => {
                            e.preventDefault();
                            this.setState({ menu : false });
                            this.clickSection('home')
                        }}>Home</a></li>
                        <li><a href="">Marketplace</a></li>
                        <li><a href="" onClick={(e) => {
                            e.preventDefault();
                            this.setState({ menu : false });
                            this.clickSection('cadastro')
                        }}>Como Funciona</a></li>
                        <li><a href="" onClick={(e) => {
                            e.preventDefault();
                            this.setState({ menu : false });
                            this.clickSection('duvidas')
                        }}>Dúvidas</a></li>
                    </ul>
                    <div className="btns">
                        <a href="">Login</a>
                        <a href="">Cadastre-se</a>
                    </div>
                </div>
            </Header>
        );
    }
}
 
export default ComponentHeader;