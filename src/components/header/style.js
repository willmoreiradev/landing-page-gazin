import styled from 'styled-components';



export const Header = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding: ${props => props.menu_fixed ? '15px 0px' : '31px 0px'};
    z-index: 30;
    background-color: ${props => props.menu_fixed ? '#ffffff' : 'transparent'};
    box-shadow: ${props => props.menu_fixed ? '0 0 30px 0 rgba(0, 0, 0, 0.1)' : 'transparent'};
    transition: all .3s;
    .container {
        display: flex;
        align-items: center;
        justify-content: space-between;
        position: relative;
        z-index: 3;
    }
    .menu-mobile {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #ffffff;
        z-index: 1;
        padding-top: 100px;
        pointer-events: ${props => props.menu_opened ? 'all' : 'none'};
        opacity: ${props => props.menu_opened ? '1' : '0'};
        transform: ${props => props.menu_opened ? 'translateY(0px)' : 'translateY(100px)'};
        transition: all .3s;
        ul {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 40px;
            li {
                margin-bottom: 35px;
                &:last-child {
                    margin-bottom: 0px;
                }
                a {
                    color: #646981;
                    font-size: 20px;
                }
            }
        }
        .btns {
            display: flex;
            flex-direction: column;
            align-items: center;
            a {
                width: 259px;
                height: 55px;
                display: flex;
                align-items: center;
                justify-content: center;
                margin-bottom: 20px;
                background-color: #FFA82E;
                border-radius: 6px;
                color: #FFFFFF;
                font-weight: bold;
                font-size: 14px;
                &:last-child {
                    margin-bottom: 0px;
                }
            }
        }
    }
    .menu-button {
        display: none;
        width: 31px;
        padding: 8px 0;
        cursor: pointer;
        transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
        &:hover {
            .menu-icon,
            .menu-icon:after {
                width: 31px;
                transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
            }
        }
        .menu-icon {
            width: 31px;
            height: ${props => props.menu_opened ? '0px' : '4px'};
            background-color: ${props => props.menu_opened ? '#0C64D3' : props.menu_fixed ? '#0C64D3' : '#FFFFFF'};
            display: block;
            position: relative;
            transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
            border-radius: 10px;
            &::before,
            &::after {
                content: '';
                display: block;
                height: 4px;
                background-color: ${props => props.menu_opened ? '#0C64D3' : props.menu_fixed ? '#0C64D3' : '#FFFFFF'};
                position: absolute;
                right: 0;
                border-radius: 10px;
                transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
            }
            &::before {
                margin-top: ${props => props.menu_opened ? '0px' : '-10px'};
                transform: ${props => props.menu_opened ? 'rotate(-45deg)' : 'rotate(0deg)'};
                width: 31px;
                transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
            }
            &::after {
                margin-top: ${props => props.menu_opened ? '0px' : '10px'};
                transform: ${props => props.menu_opened ? 'rotate(45deg)' : 'rotate(0deg)'};
                width: 31px;
                transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
            }
        }
    }
  .esq {
      display: flex;
      align-items: center;
      .logo {
          display: block;
          margin-right: 87px;
      }
      nav {
          ul {
              display: flex;
              li {
                  margin-left: 40px;
                  &:first-child {
                      margin-left: 0px;
                  }
                  a {
                    color: #646981;
                    font-size: 14px;
                    transition: all .3s;
                    &:hover {
                        transition: all .3s;
                        color: #FFA82E;
                    }
                  }
              }
          }
      }
  }
  .dir {
      position: relative;
      z-index: 1;
      display: flex;
      align-items: center;
      .btn {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 46px;
        border-radius: 4px;
        font-weight: 600;
        font-size: 14px;
        color: #FFFFFF;
        &.login {
            border: ${props => props.menu_fixed ? '1px solid #4a97fa' : '1px solid #FFFFFF'};
            color: ${props => props.menu_fixed ? '#4a97fa' : '#FFFFFF'};
            width: 122px;
            margin-right: 16px;
            transition: all .3s;
            &:hover {
                background-color: #FFFFFF;
                color: #4a97fa;
                font-weight: bold;
                transition: all .3s;
            }
        }
        &.cadastro {
            width: 160px;
            background-color: #FFA82E;
            border: 1px solid #FFA82E;
            transition: all .3s;
            &:hover {
                color: #FFA82E;
                font-weight: bold;
                background-color: transparent;
                transition: all .3s;
            }
        }
      }
  }
  @media(max-width: 1600px) {
      .esq {
          .logo {
            margin-right: 50px;
          }
          nav {
              ul {
                  li {
                    margin-left: 35px;
                    a {
                        font-size: 13px;
                    }
                  }
              }
          }
      }
  }
  @media(max-width: 1100px) {
      .menu-button {
          display: block;
      }
      .esq {
          nav {
              display: none;
          }
      }
      .dir {
          display: none;
      }
  }
  @media(max-width: 480px) {
    padding: 15px 0px;
  }
`;