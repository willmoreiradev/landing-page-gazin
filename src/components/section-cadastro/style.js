import styled from 'styled-components';

export const SectionCadastro = styled.section`
  padding-top: 83px;
  padding-bottom: 122px;
  .title {
      margin-bottom: 50px;
      span {
        background: linear-gradient(to right, #0C64D3 0%, #2A89FF 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        font-weight: bold;
        font-size: 14px;
        line-height: 24px;
        letter-spacing: 3px;
        text-transform: uppercase;
        display: block;
        margin-bottom: 6px;
      }
      h2 {
        font: normal bold 32px/1 'Montserrat';
        color: #363843;
      }
  }
  .all {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .card {
        position: relative;
        height: 447px;
        padding-top: 86px;
        .number {
            position: absolute;
            left: -45px;
            top: 50px;
        }
        .icone {
            margin-bottom: 48px;
        }
        h3 {
            font: normal bold 18px/22px 'Montserrat';
            color: #474B5B;
            margin-bottom: 21px;
        }
        p {
            max-width: 237px;
            font-size: 14px;
            line-height: 22px;
            color: #474B5B;
        }
        &:first-child {
            .icone {
                margin-bottom: 55px;
            }
            p {
                max-width: 209px;
            }
        }
        &:nth-child(2) {
            p {
                max-width: 237px;
            }
        }
        &:nth-child(3) {
            .icone {
                margin-bottom: 65px;
            }
            p {
                max-width: 201px;
            }
        }
        &:last-child {
            width: 358px;
            background: linear-gradient(180deg, #0C64D3 0%, #2A89FF 100%);
            box-shadow: 0px 14px 36px rgba(57, 137, 237, 0.26);
            border-radius: 14px;
            padding: 86px 70px;
            h3 {
                color: #FFFFFF;
            }
            p {
                color: #FFFFFF;
            }
            a {
                width: 100%;
                height: 55px;
                background-color: #FFFFFF;
                border-radius: 6px;
                display: flex;
                align-items: center;
                justify-content: space-between;
                padding-left: 29px;
                color: #474B5B;
                font-size: 14px;
                font-weight: bold;
                margin-top: 29px;
                transform: scale(1);
                transition: all .3s;
                span {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    border-left: 1px solid rgba(71, 75, 91, 0.1);
                    width: 58px;
                    height: 100%;
                }
                &:hover {
                    transform: scale(1.04);
                    transition: all .3s;
                }
            }
        }
    }
  }
  .details {
    position: absolute;
    bottom: -50px;
    right: -30px;
  }
  @media(max-width: 1100px) {
    padding-bottom: 40px;
      .title {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      .all {
        flex-direction: column;
        align-items: flex-start;
        .card {
            position: relative;
            height: auto;
            padding-top: 0;
            margin-bottom: 50px;
            width: 100%;
            display: flex;
            align-items: flex-start;
            &:last-child {
                width: 100%;
                height: auto;
                padding: 30px;
                flex-direction: column;
                .icone {
                    margin-right: 0;
                    margin-bottom: 20px !important;
                }
            }
            .number {
                position: absolute;
                left: 0;
                top: 0;
            }
            .icone {
                margin-bottom: 0 !important;
                margin-right: 30px;
            }
            p {
                max-width: 100% !important;
            }
        }
      }
  }
  @media(max-width: 480px) {
    padding-top: 40px;
    .title {
        h2 {
            text-align: center;
            font-size: 28px;
            line-height: 32px;
        }
    }
    .all {
        .card {
            margin-bottom: 36px;
            .icone {
                margin-right: 26px;
                max-width: 70px;
            }
            .number {
                max-width: 30px;
                top: -25px;
            }
            h3 {
                font-size: 16px;
                line-height: 20px;
                margin-bottom: 12px;
            }
            p {
                font-size: 13px;
                line-height: 19px;
            }
        }
    }
  }
`;