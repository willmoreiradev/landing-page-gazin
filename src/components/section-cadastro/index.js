import React, { Component } from 'react'
import { SectionCadastro } from './style';
import { Container } from '../../Style/global';

import IconeBalao from '../../images/icone-balao.svg';
import ArrowRightBlack from '../../images/arrow-right-black.svg';
import IconeConfirmacao from '../../images/icone-confirmacao-dados.svg';
import IconePronto from '../../images/icone-pronto.svg';
import IconeComecar from '../../images/icone-comecar.svg';
import Details from '../../images/details-register.svg';
import NumberOne from '../../images/number-one.svg';
import NumberTwo from '../../images/numberTwo.svg';
import NumberThree from '../../images/numberThree.svg';

class SCadastro extends Component {
    state = {  }
    render() { 
        return ( 
            <SectionCadastro id="cadastro">
                <Container>
                    <div className="title" data-aos='fade-up'>
                        <span>como cadastrar</span>
                        <h2>Multiplique suas vendas com a Gazin</h2>
                    </div>
                    <div className="all" data-aos='fade-left'>
                        <div className="card">
                            <img src={NumberOne}  className="number"/>
                            <img src={IconeBalao} className="icone"/>
                            <div>
                                <h3>Conte-nos quem é você</h3>
                                <p>Deixe que nós conheçamos você e sua empresa melhor, preencha nosso formulário de cadastro com seus dados.</p>
                            </div>
                        </div>
                        <div className="card">
                            <img src={NumberTwo}  className="number"/>
                            <img src={IconeConfirmacao} className="icone"/>
                            <div>
                                <h3>Confirmação de dados</h3>
                                <p>Seus dados passarão por uma validação interna para verificarmos se você não se esqueceu de nenhuma informação.</p>
                            </div>
                        </div>
                        <div className="card">
                            <img src={NumberThree}  className="number"/>
                            <img src={IconePronto} className="icone"/>
                            <div>
                                <h3>Tudo pronto!</h3>
                                <p>Entraremos em contato para te informar melhor como deve prosseguir em seus primeiros passos como parceiro Gazin.</p>
                            </div>
                        </div>
                        <div className="card">
                            <div>
                                <img src={IconeComecar} className="icone"/>
                                <h3>Comece agora</h3>
                                <p>Ofereça seus produtos na Gazin</p>
                            </div>
                            <a href="" target="_blank">Eu quero <span><img src={ArrowRightBlack}/></span></a>
                        </div>
                    </div>
                    <img src={Details} className="details"/>
                </Container>
            </SectionCadastro>    
        );
    }
}
 
export default SCadastro;