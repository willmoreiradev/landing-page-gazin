import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ModalVideo from 'react-modal-video'
import { StyleSectionConfianca } from './style';

import Logo from '../../images/logo.svg';
import IconeCanaisVenda from '../../images/icone-canais-venda.svg';
import IconePlay from '../../images/icone-play.svg';
import IconeInvestimentoInicial from '../../images/icone-investimento-inicial.svg';
import IconeVolume from '../../images/icone-volume.svg';
import IconeComissao from '../../images/icone-comissao.svg';

import '../../Style/modal-video.min.css';

class SConfianca extends Component {
    constructor () {
        super()
        this.state = {
            isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }
    
    openModal () {
        this.setState({isOpen: true})
    }
    render() { 
        return ( 
            <StyleSectionConfianca>
                <figure data-aos='fade-right'>
                    <div className="box">
                        <img src={Logo}/>
                    </div>
                </figure>
                <div className="texto" data-aos='fade-left'>
                    <div className="title">
                        <span>Confie em quem está no mercado </span>
                        <h2>com mais de 50 anos</h2>
                    </div>
                    <ul>
                        <li>
                            <div className="icone">
                                <img src={IconeCanaisVenda}/>
                            </div>
                            <div className="info">
                                <h3>Conte com nossos<br/> canais de vendas</h3>
                                <p>Divulgue seus produtos em todos<br/> os nossos canais de venda.</p>
                            </div>
                        </li>
                        <li>
                            <div className="icone">
                                <img src={IconeInvestimentoInicial}/>
                            </div>
                            <div className="info">
                                <h3>Sem investimento inicial</h3>
                                <p>Você não precisa gastar nada para se<br/> tornar um parceiro, assim você<br/> investe seu dinheiro no seu negócio.</p>
                            </div>
                        </li>
                        <li>
                            <div className="icone">
                                <img src={IconeComissao}/>
                            </div>
                            <div className="info">
                                <h3>Comissão apenas sobre venda</h3>
                                <p>Com a gente o que é certo, é<br/> certo. Teremos nossa comissão só<br/> no que realmente vender.</p>
                            </div>
                        </li>
                        <li>
                            <div className="icone">
                                <img src={IconeVolume}/>
                            </div>
                            <div className="info">
                                <h3>Maior volume de vendas</h3>
                                <p>Juntando-se a nós seus produtos<br/> estarão disponíveis para muito<br/> mais gente.</p>
                            </div>
                        </li>
                    </ul>
                    <div className="box-video">
                        <div>
                            <h4>A Gazin a seu dispor</h4>
                            <p>Nossa empresa trabalhando pela sua.</p>
                        </div>
                        <button onClick={this.openModal}>
                            <img src={IconePlay}/>
                            <span>Assista o <strong>video</strong></span>
                        </button>
                    </div>
                </div>
                <ModalVideo channel='youtube' isOpen={this.state.isOpen} videoId='pbzeZPRDmaU' onClose={() => this.setState({isOpen: false})} />
            </StyleSectionConfianca>
        );
    }
}
 
export default SConfianca;