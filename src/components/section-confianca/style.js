import styled from 'styled-components';

import FachadaGazin from '../../images/fachada-gazin.jpg';

export const StyleSectionConfianca = styled.section`
    position: relative;
    display: flex;
    align-items: center;
    padding-bottom: 105px;
    figure {
        background: url(${FachadaGazin}) no-repeat center center;
        position: relative;
        width: 36%;
        height: 783px;
        margin-right: 6%;
        .box {
            background-color: #FFFFFF;
            box-shadow: 0px 4px 34px rgba(0, 0, 0, 0.11);
            border-radius: 22px;
            position: absolute;
            bottom: 81px;
            right: -46px;
            width: 316px;
            height: 128px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    }
    .texto {
        width: 794px;
        .title {
            margin-bottom: 75px;
            span {
                font-family: 'Montserrat';
                font-size: 24px;
                color: #363843;
            }
            h2 {
                font: normal bold 48px/1.1 'Montserrat';
                color: #363843;
            }
        }
        ul {
            display: grid;
            align-items: flex-start;
            grid-template-columns: 1fr 1fr;
            grid-column-gap: 65px;
            grid-row-gap: 62px;
            margin-bottom: 75px;
            li {
                display: flex;
                align-items: center;
                .icone {
                    margin-right: 25px;
                    background: rgba(255, 168, 46, 0.14);
                    width: 73px;
                    height: 73px;
                    border-radius: 18px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
                .info {
                    h3 {
                        font: normal bold 16px/20px 'Montserrat';
                        color: #363843;
                        margin-bottom: 8px;
                    }
                    p {
                        font-size: 14px;
                        line-height: 24px;
                        color: #646981;
                    }
                }
            }
        }
        .box-video {
            background-color: #FFA82E;
            border-radius: 22px;
            width: 100%;
            height: 128px;
            display: flex;
            align-items: center;
            justify-content: center;
            h4 {
                font: normal bold 24px/29px 'Montserrat';
                color: #FFFFFF;
            }
            p {
                font-size: 14px;
                line-height: 24px;
                color: #FFFFFF;
            }
            button {
                width: 327px;
                height: 70px;
                background-color: #FFFFFF;
                box-shadow: 0px 4px 14px rgba(255, 255, 255, 0.11);
                border-radius: 12px;
                margin-left: 66px;
                display: flex;
                align-items: center;
                justify-content: center;
                img {
                    margin-right: 27px;
                }
                span {
                    font: normal normal 18px/24px 'Montserrat';
                    color: #FFA82E;
                    strong {
                        font: normal bold 18px/24px 'Montserrat';
                        color: #FFA82E;
                    }
                }
            }
        }
    }
    @media(max-width: 1600px) {
        figure {
            height: 555px;
            background-size: 100%;
            .box {
                width: 280px;
                height: 95px;
            }
        }
        .texto {
            width: 690px;
            .title {
                margin-bottom: 50px;
                span {
                    font-size: 20px;
                }
                h2 {
                    font-size: 40px;
                }
            }
            ul {
                grid-column-gap: 40px;
                grid-row-gap: 40px;
                margin-bottom: 45px;
                li {
                    .icone {
                        width: 60px;
                        height: 60px;
                        margin-right: 20px;
                    }
                    .info {
                        h3 {
                            font-size: 15px;
                            margin-bottom: 5px;
                        }
                        p {
                            font-size: 13px;
                            line-height: 19px;
                        }
                    }
                }
            }
            .box-video {
                height: 100px;
                h4 {
                    font-size: 22px;
                }
                p {
                    font-size: 13px;
                }
                button {
                    width: 280px;
                    height: 60px;
                }
            }
        }
    }
    @media(max-width: 1100px) {
        flex-direction: column;
        align-items: flex-start;
        padding-bottom: 40px;
        figure {
            height: 390px;
            background-size: 100%;
            width: 90%;
        }
        .texto {
            width: 100%;
            padding: 0px 20px;
            padding-top: 50px;
            .title {
                display: flex;
                flex-direction: column;
                align-items: center;
            }
        }
    }
    @media(max-width: 480px) {
        padding-top: 0;
        padding-bottom: 80px;
        flex-direction: column-reverse;
        figure {
            margin-top: 72px;
            height: 300px;
            width: 100%;
            .box {
                width: 204px;
                height: 82px;
                right: initial;
                left: 50%;
                margin-left: -102px;
                bottom: -40px;
            }
        }
        .texto {
            padding-top: 0;
            .title {
                margin-bottom: 43px;
                span {
                    text-align: center;
                    font-size: 16px;
                    margin-bottom: 13px;
                }
                h2 {
                    text-align: center;
                    font-size: 36px;
                    max-width: 266px;
                }
            }
            ul {
                grid-template-columns: 1fr;
                grid-row-gap: 36px;
            }
            .box-video {
                height: auto;
                padding: 29px 24px;
                flex-direction: column;
                align-items: center;
                padding: 20px 0px;
                padding-bottom: 27px;
                button {
                    margin-left: 0;
                    margin-top: 27px;
                }
            }
        }
    }
`;