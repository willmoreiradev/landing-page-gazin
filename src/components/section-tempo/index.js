import React, { Component } from 'react'
import { StyleSectionTempo } from './style';
import { Container } from '../../Style/global';

import Iphone from '../../images/iphone.png';
import Facebook from '../../images/facebook.svg';
import Youtube from '../../images/youtube.svg';
import Linkedin from '../../images/linkedin.svg';
import IconPhone from '../../images/icon-phone.svg';

class STempo extends Component {
    state = {  }
    render() { 
        return ( 
            <StyleSectionTempo>
                <Container>
                   <div className="cont">
                    <img src={Iphone} className="img" data-aos='zoom-in'/>
                    <div className="texto" data-aos='fade-left'>
                        <h2>Não perca tempo e venha para a Gazin</h2>
                        <p>Amplie sua rede de contatos e conquentemente o potencial de faturamento da sua empresa.</p>
                        <div className="contato">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/grupogazin" target="_blank">
                                        <img src={Facebook}/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/user/gazinoficial" target="_blank">
                                        <img src={Youtube}/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/2450400?trk=tyah&trkInfo=tarId%3A1421946155243%2Ctas%3AGazin%2Cidx%3A2-1-6" target="_blank">
                                        <img src={Linkedin}/>
                                    </a>
                                </li>
                            </ul>
                            <a href="tel:+55-0800-644-9292" className="phone"><img src={IconPhone}/> 0800 644 9292</a>
                        </div>
                    </div>
                   </div>
                </Container>
            </StyleSectionTempo>
        );
    }
}
 
export default STempo;