import styled from 'styled-components';

import BgTempo from '../../images/bg-tempo.jpg';

export const StyleSectionTempo = styled.section`
  background: url(${BgTempo}) no-repeat center center;
  height: 551px;
  .cont {
    display: flex;
    align-items: flex-start;
    justify-content: flex-end;
  }
  .img {
    position: absolute;
    left: 0px;
    top: -135px;
  }
  .texto {
      padding-top: 124px;
      width: 400px;
      h2 {
            font: normal bold 36px/1.2 'Montserrat';
            color: #FFFFFF;
            margin-bottom: 14px;
      }
      p {
        max-width: 370px;
        font-size: 14px;
        line-height: 24px;
        color: #FFFFFF;
        margin-bottom: 40px;
      }
      .contato {
          display: flex;
          align-items: center;
          ul {
              display: flex;
              align-items: center;
              margin-right: 31px;
              li {
                  margin-left: 12px;
                  &:first-child {
                      margin-left: 0px;
                  }
                  a {
                    width: 46px;
                    height: 46px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    background-color: rgba(255, 255, 255, 0.2);
                    border-radius: 12px;
                    transform: scale(1);
                    transition: all .3s;
                    &:hover {
                      transform: scale(1.04);
                      transition: all .3s;
                    }
                  }
              }
          }
          .phone {
            display: flex;
            align-items: center;
            font-weight: 500;
            font-size: 15px;
            line-height: 24px;
            color: #FFFFFF;
            transition: all .3s;
            img {
                margin-right: 11px;
            }
            &:hover {
              color: #FFA82E;
              transition: all .3s
            }
          }
      }
  }
  @media(max-width: 1100px) {
    height: auto;
    padding-bottom: 100px;
    background-size: cover;
    .cont {
      flex-direction: column;
      align-items: center;
    }
    .img {
      position: relative;
      top: 0;
      position: relative;
      top: 0;
      margin-top: -130px;
    }
    .texto {
      padding-top: 0;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-top: -100px;
      p {
        text-align: center;
      }
    }
  }
  @media(max-width: 480px) {
    padding-bottom: 75px;
    .img {
      margin-left: -50px;
      margin-bottom: 40px;
    }
    .texto {
      h2 {
        text-align: center;
        font-size: 28px;
        margin-bottom: 20px;
      }
      p {
        max-width: 294px;
        margin-bottom: 32px;
      }
    }
    .contato {
      flex-direction: column;
      align-items: center;
      ul {
        margin-right: 0;
        margin-bottom: 30px;
      }
    }
  }
`;