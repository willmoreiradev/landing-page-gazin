import styled from 'styled-components';

export const StyleFooter = styled.footer`
    position: relative;
    background-color: #0D71F0;
    padding-top: 71px;
    padding-bottom: 56px;
    .btn-scroll {
        width: 51px;
        height: 51px;
        background-color: #ffffff;
        box-shadow: 0px 4px 34px rgba(0, 0, 0, 0.15);
        border-radius: 12px;
        display: flex;
        align-items: center;
        justify-content: center;
        position: absolute;
        top: 0;
        left: 50%;
        margin-left: -25.5px;
        margin-top: -25.5px;
        transform: scale(1);
        transition: all .3s;
        &:hover {
            transform: scale(1.04);
            transition: all .3s;
        }
    }
    .container {
        display: flex;
        align-items: flex-end;
        justify-content: space-between;
    }
    .esq {
        img {
            margin-bottom: 36px;
        }
        p {
            font-size: 14px;
            line-height: 24px;
            color: #FFFFFF;
        }
    }
    .dir {
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        nav {
            display: flex;
            align-items: center;
            margin-bottom: 33px;
            ul {
                display: flex;
                margin-right: 95px;
                li {
                    margin-left: 51px;
                    &:first-child {
                        margin-left: 0px;
                    }
                    a {
                        font: normal bold 14px/1.2 'Inter';
                        color: #FFFFFF;
                        transition: all .3s;
                        &:hover {
                            color: #FFA82E;
                            transition: all .3s;
                        }
                    }
                }
            }
            .btns {
                display: flex;
                align-items: center;
                .btn {
                    width: 189.28px;
                    height: 55px;
                    background-color: #FFA82E;
                    border: 2px solid #FFA82E;
                    border-radius: 6px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    margin-right: 27px;
                    font-family: 'Montserrat';
                    font-weight: bold;
                    font-size: 14px;
                    color: #FFFFFF;
                    transition: all .3s;
                    &:hover {
                        background-color: transparent;
                        color: #FFA82E;
                        transition: all .3s;
                    }
                    &:last-child {
                        margin-right: 0px;
                    }
                }
            }
        }
        .dev {
            display: flex;
            align-items: center;
            li {
                &:first-child {
                    padding-right: 20px;
                    margin-right: 20px;
                    border-right: 1px solid rgba(255, 255, 255, 0.2);
                }
            }
        }
    }
    @media(max-width: 1100px) {
        padding-top: 80px;
        padding-bottom: 40px;
        .container {
            flex-direction: column;
            align-items: center;
        }
        .esq {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 30px;
        }
        .dir {
            flex-direction: column;
            nav {
                flex-direction: column;
                align-items: center;
                ul {
                    margin-right: 0;
                    margin-bottom: 30px;
                }
            }
            .dev {
                width: 100%;
                justify-content: center;
            }
        }
    }
    @media(max-width: 480px) {
        .esq {
            p {
                text-align: center;
                max-width: 240px;
            }
        }
        .dir {
            nav {
                .btns {
                    flex-direction: column;
                    align-items: center;
                    .btn {
                        margin-right: 0;
                        margin-bottom: 15px;
                        width: 259px;
                    }
                }
            }
        }
    }
`;