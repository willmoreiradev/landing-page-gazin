import React, { Component } from 'react'
import { Link } from 'gatsby'
import { StyleFooter } from './style';
import { Container } from '../../Style/global';

import Logo from '../../images/logo.svg';
import eCode from '../../images/logo-ecode.svg';
import Insany from '../../images/logo-insany.svg';
import ArrowUp from '../../images/arrow-up-blue.svg';

class Footer extends Component {
    clickSection = (section) => {
        const tabScroll = document.getElementById(section);
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': tabScroll.offsetTop - 80
        });   
    }
    render() { 
        return ( 
            <StyleFooter>
                <button className="btn-scroll" 
                onClick={(e) => {
                    e.preventDefault();
                    this.clickSection('home')
                }}>
                    <img src={ArrowUp}/>
                </button>
                <Container className="container">
                    <div className="esq">
                        <img src={Logo}/>
                        <p>Copyright © Gazin S A. Todos os direitos reservados.</p>
                    </div>
                    <div className="dir">
                        <nav>
                            <ul>
                                <li><a href="www.grupogazin.com.br" target="_blank">Sobre a Gazin</a></li>
                                <li><a href="">Desenvolvedores</a></li>
                            </ul>
                            <div className="btns">
                                <a className="btn" href="" target="_blank">Contrato</a>
                                <Link className="btn" to="/cadastro">Cadastre-se</Link>
                            </div>
                        </nav>
                        <ul className="dev">
                            <li>
                                <a href="https://www.agenciaecode.com.br/" target="_blank">
                                    <img src={eCode}/>
                                </a>
                            </li>
                            <li>
                                <a href="https://insanydesign.com/" target="_blank">
                                    <img src={Insany}/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </Container>
            </StyleFooter>
        );
    }
}
 
export default Footer;

