import styled from 'styled-components';

import ArrowSelect from '../../../images/arrow-select.svg';

export const StyleAreaCadastro = styled.section`
    padding-top: 63px;
    padding-bottom: 148px;
    h2 {
        font-weight: bold;
        font-size: 17px;
        line-height: 26px;
        text-align: center;
        color: #363843;
        margin-bottom: 10px;
    }
    p {
        max-width: 400px;
        margin: 0 auto;
        text-align: center;
        margin-bottom: 27px;
        font-size: 14px;
        line-height: 17px;
        color: #646981;
    }
    .formulario {
        .form-group {
            margin-bottom: 24px;
            &:last-child {
                margin-bottom: 0px;
            }
        }
        .btns {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 20px;
            button {
                margin: 0px;
            }
        }
        .options {
            display: flex;
            input[type=radio] {
                margin-left: 85px;
                margin-right: 8px;
                &:first-child {
                    margin-left: 0px;
                }
            }
        }
        label {
            display: block;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            color: #363843;
            margin-bottom: 11px;
        }
        input[type=text] {
            appearance: none;
            width: 100%;
            height: 54px;
            border: 1px solid #DCDDE3;
            border-radius: 6px;
            padding: 0px 18px;
            font-size: 14px;
            color: #A0A4B3;
        }
        select {
            background: url(${ArrowSelect}) no-repeat #ffffff right 18px center;
            appearance: none;
            width: 100%;
            height: 54px;
            border: 1px solid #DCDDE3;
            border-radius: 6px;
            padding: 0px 18px;
            font-size: 14px;
            color: #A0A4B3;
        }
        button {
            width: 100%;
            height: 56px;
            margin-top: 25px;
            background-color: #FFA82E;
            border: 2px solid #FFA82E;
            border-radius: 6px;
            color: #FFFFFF;
            font-size: 16px;
            font-weight: 600;
            transition: all .3s;
            &:hover {
                background-color: transparent;
                color: #FFA82E;
                transition: all .3s
            }
            &.btn-back {
                background-color: transparent;
                color: #FFA82E;
                transition: all .3s;
                &:hover {
                    background-color: #FFA82E;
                    color: #FFFFFF;
                }
            }
        }
    }
    @media(max-width: 1200px) {
        padding: 40px 0px;
    }
    @media(max-width: 480px) {
        h2 {
            font-size: 20px;
        }
        p {
            max-width: 270px;
            line-height: 22px;
            br {
                display: none;
            }
        }
    }
`;

export const ContainerForm = styled.div`
    width: 467px;
    margin: 0 auto;
    @media(max-width: 1200px) {
        width: 100%;
        padding: 0px 15px;
    }
`;

export const StepOne = styled.div`
  display: ${props => props.show_first_step ? 'block' : 'none'};
`;

export const StepTwo = styled.div`
  display: ${props => props.show_second_step ? 'block' : 'none'};
`;

export const StepThree = styled.div`
  display: ${props => props.show_three_step ? 'block' : 'none'};
`;

export const InputIE = styled.div`
    margin-bottom: 24px;
    display: ${props => props.visible_ie ? 'block' : 'none'};
`;

export const RadioIsento = styled.div`
    display: flex;
    align-items: center;
    cursor: pointer;
    .check {
        width: 20px;
        height: 20px;
        border: 1px solid #DCDDE3;
        border-radius: 50%;
        margin-right: 12px;
        display: flex;
        align-items: center;
        justify-content: center;
        &:before {
            content: "";
            width: 10px;
            height: 10px;
            border-radius: 50%;
            display: inline-block;
            background: #3e81ff;
            transform: ${props => props.checked ? 'scale(1)' : 'scale(0)'};
            transition: all .3s;
        }
    }
    span {
        color: #646981;
    }
`;


export const RegisterSuccess = styled.div`
  display: ${props => props.show_success ? 'block' : 'none'};
  ${ContainerForm} {
    display: flex;
    flex-direction: column;
    align-items: center;
    img {
        margin-bottom: 55px;
    }
    h2 {
        font-weight: bold;
        font-size: 28px;
        line-height: 35px;
        text-align: center;
        letter-spacing: -0.114286px;
        color: #000000;
        margin-bottom: 22px;
    }
    p {
        max-width: 326px;
        margin-bottom: 39px;
    }
    a {
        width: 281px;
        height: 58px;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: #FFA82E;
        border: 2px solid #FFA82E;
        border-radius: 8px;
        color: #FFFFFF;
        font-weight: 600;
        text-transform: uppercase;
        transition: all .3s;
        &:hover {
            background-color: transparent;
            color: #FFA82E;
            transition: all .3s;
        }
    }
  }
  @media(max-width: 480px) {
      img {
        max-width: 100px;
        margin-bottom: 20px;
      }
      h2 {
        font-size: 24px;
        max-width: 250px;
        line-height: 30px;
        margin-bottom: 15px;
      }
      p {
        max-width: 100%;
        margin-bottom: 20px;
      }
      a {
          width: 100% !important;
      }
  }
`;