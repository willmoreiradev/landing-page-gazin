import React, { Component } from 'react'
import { Link } from 'gatsby'

import SuccessIcon from '../../../images/icone-sucesso.svg';

import { StyleAreaCadastro, 
            ContainerForm, 
            StepOne, 
            StepTwo, 
            StepThree, 
            InputIE, 
            RadioIsento, 
            RegisterSuccess } from './style';

class ComponentAreaCadastro extends Component {
    state = { 
        first_step : true,
        second_step : false,
        three_step : false,
        success: false,
        visible_input_ie: true,
        checked_ie: false
    }
    activeStep = () => {
        let StepActive = document.querySelectorAll('header .esq ul li.active');
        
        StepActive.forEach(step => {
            if(StepActive.length + 1 <= 3) {
                step.nextElementSibling.classList.add('active');
            }
        })
    }
    RemoveStep = () => {
        let StepActive = document.querySelectorAll('header .esq ul li.active');
        
        StepActive[StepActive.length - 1].classList.remove('active');
    }
    render() { 
        return ( 
            <StyleAreaCadastro>
                <form>
                    <StepOne show_first_step={this.state.first_step}>
                        <ContainerForm>
                            <h2>Você está preparado para fazer decolar suas vendas?</h2>
                            <p>Preencha os campos abaixo com os dados da sua empresa.</p>
                            <div className="formulario">
                                <div className="form-group">
                                    <label>CNPJ</label>
                                    <input type="text" placeholder="Seu CNPJ"/>
                                </div>
                                <div className="form-group">
                                    <RadioIsento checked={this.state.checked_ie} onClick={() => {
                                        this.setState({ visible_input_ie : !this.state.visible_input_ie });
                                        this.setState({ checked_ie : !this.state.checked_ie })
                                    }}>
                                        <div className="check"></div>
                                        <span>Sou isento</span>
                                    </RadioIsento>
                                </div>
                                <InputIE visible_ie={this.state.visible_input_ie}>
                                    <label>Inscrição estadual</label>
                                    <input type="text" placeholder="Sua Inscrição estadual"/>
                                </InputIE>
                                <div className="form-group">
                                    <label>Site</label>
                                    <input type="text" placeholder="Seu Site"/>
                                </div>
                                <div className="form-group">
                                    <button type="button" onClick={() => {
                                        this.setState({ first_step : false });
                                        this.setState({ second_step : true });
                                        this.activeStep();
                                    }}>Avançar</button>
                                </div>
                            </div>
                        </ContainerForm>
                    </StepOne>
                    <StepTwo show_second_step={this.state.second_step}>
                        <ContainerForm>
                            <h2>Conte pra gente um pouquinho sobre você</h2>
                            <p>Deixe aqui as informações necessárias para que possamos entrar em contato com você.</p>
                            <div className="formulario">
                                <div className="form-group">
                                    <label>CPF</label>
                                    <input type="text" placeholder="Seu CPF"/>
                                </div>
                                <div className="form-group">
                                    <label>Nome</label>
                                    <input type="text" placeholder="Nome"/>
                                </div>
                                <div className="form-group">
                                    <label>Telefone</label>
                                    <input type="text" placeholder="Telefone"/>
                                </div>
                                <div className="form-group">
                                    <label>Cargo</label>
                                    <select>
                                        <option>Designer</option>
                                        <option>Front-End</option>
                                        <option>Back-End</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <div className="btns">
                                        <button type="button" className="btn-back" onClick={() => {
                                            this.setState({ first_step : true });
                                            this.setState({ second_step : false });
                                            this.setState({ three_step : false });
                                            this.RemoveStep();
                                        }}>Voltar</button>
                                        <button type="button" onClick={() => {
                                            this.setState({ first_step : false });
                                            this.setState({ second_step : false });
                                            this.setState({ three_step : true });
                                            this.activeStep();
                                        }}>Avançar</button>
                                    </div>
                                </div>
                            </div>
                        </ContainerForm>
                    </StepTwo>
                    <StepThree show_three_step={this.state.three_step}>
                        <ContainerForm>
                            <h2>Agora estamos quase lá...</h2>
                            <p>Por último precisamos saber sobre tecnologias<br/> necessárias para integrar o seu sistema </p>
                            <div className="formulario">
                                <div className="form-group">
                                    <label>Existem departamento de TI </label>
                                    <div className="options">
                                        <input type="radio" value="sim" name="dep_ti" /> Sim
                                        <input type="radio" value="nao" name="dep_ti" checked/> Não
                                    </div>
                                    
                                </div>
                                <div className="form-group">
                                    <label>Possui ERP</label>
                                    <select>
                                        <option>Enterprise Resource Planning</option>
                                        <option>Enterprise Resource Planning</option>
                                        <option>Enterprise Resource Planning</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label>Possui Integradora</label>
                                    <select>
                                        <option>Enterprise Gazin</option>
                                        <option>Enterprise Gazin</option>
                                        <option>Enterprise Gazin</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <div className="btns">
                                        <button type="button" className="btn-back" onClick={() => {
                                            this.setState({ first_step : false });
                                            this.setState({ second_step : true });
                                            this.setState({ three_step : false });
                                            this.RemoveStep();
                                        }}>Voltar</button>
                                        <button type="button" onClick={() => {
                                            this.setState({ first_step : false });
                                            this.setState({ second_step : false });
                                            this.setState({ three_step : false });
                                            this.setState({ success : true });
                                            this.activeStep();
                                        }}>Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </ContainerForm>
                    </StepThree>
                    <RegisterSuccess show_success={this.state.success}>
                        <ContainerForm>
                            <img src={SuccessIcon}/>
                            <h2>Cadastro concluído com sucesso</h2>
                            <p>Logo entraremos em contato com você para prosseguir com o cadastro.</p>
                            <Link to="/">Voltar para a home</Link>
                        </ContainerForm>
                    </RegisterSuccess>
                </form>
            </StyleAreaCadastro>
        );
    }
}
 
export default ComponentAreaCadastro;