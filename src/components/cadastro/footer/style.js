import styled from 'styled-components';
import { Container } from '../../../Style/global';


export const StyleFooter = styled.footer`
    border-top: 1px solid #E8E8E8;
    padding: 34px 0px;
    ${Container} {
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
    }
    .phone {
        display: flex;
        align-items: center;
        img {
            margin-right: 29px;
        }
        .info {
            span {
                display: block;
                font-weight: 600;
                font-size: 14px;
                line-height: 17px;
                display: block;
                letter-spacing: -0.1px;
                color: #646981;
            }
            a {
                font-weight: bold;
                font-size: 24px;
                line-height: 29px;
                letter-spacing: -0.171429px;
                color: #363843;
                margin-top: 3px;
                margin-bottom: 6px;
                display: block;
            }
            p {
                color: #646981;
                font-size: 13px;
            }
        }
    }
    span {
        color: #646981;
        font-size: 14px;
    }
    ul {
        display: flex;
        align-items: center;
        margin-top: 21px;
        li {
            margin-left: 24px;
            &:first-child {
                margin-left: 0px;
            }
        }
    }
    .compra-segura {
        margin-top: 10px;
    }
    .google {
        margin-top: 15px;
    }
    @media(max-width: 1200px) {
        ${Container} {
            flex-direction: column;
            align-items: center;
        }
        span {
            display: block;
            text-align: center;
        }
        .item {
            margin-bottom: 40px;
            &:last-child {
                margin-bottom: 0px;
            }
        }
    }
`;
