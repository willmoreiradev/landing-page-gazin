import React, { Component } from 'react'

import { StyleFooter } from './style';

import { Container } from '../../../Style/global';

import IconePhone from '../../../images/icone-phone.svg';
import Visa from '../../../images/visa.png';
import MasterCard from '../../../images/mastercard.png';
import HiperCard from '../../../images/hipercard.png';
import Diners from '../../../images/diners-club.png';
import Hiper from '../../../images/hiper.png';
import Boleto from '../../../images/boleto.png';
import CompraSegura from '../../../images/compra-segura.png';
import Google from '../../../images/google.png';

class FooterCadastro extends Component {
    render() { 
        return ( 
            <StyleFooter>
                <Container>
                    <div className="item">
                        <div className="phone">
                            <div className="phone">
                                <img src={IconePhone}/>
                            </div>
                            <div className="info">
                                <span>Atendimento Gazin</span>
                                <a href="">(44) 3046-2303</a>
                                <p>de Seg à Sex das 8:30 às 18:00</p>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <span>Formas de pagamentos</span>
                        <ul>
                            <li><img src={Visa}/></li>
                            <li><img src={MasterCard}/></li>
                            <li><img src={HiperCard}/></li>
                            <li><img src={Diners}/></li>
                            <li><img src={Hiper}/></li>
                            <li><img src={Boleto}/></li>
                        </ul>
                    </div>
                    <div className="item">
                        <span>Compra segura</span>
                        <img src={CompraSegura} className="compra-segura"/>
                    </div>
                    <div className="item">
                        <span>Avaliação dos consumidores</span>
                        <img src={Google} className="google"/>
                    </div>
                </Container>
            </StyleFooter>  
        ); 
    }
}
 
export default FooterCadastro;