import styled from 'styled-components';

import { Container } from '../../../Style/global';

import ArrowStep from '../../../images/arrow-step.svg';

export const StyleHaderCadastro = styled.header`
    background: linear-gradient(87.02deg, #5C33FF -4.51%, #3399FF 104.58%);
    width: 100%;
    height: 100px;
    display: flex;
    align-items: center;
    ${Container} {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .esq {
        display: flex;
        align-items: center;
        ul {
            display: flex;
            margin-left: 127px;
            li {
                position: relative;
                margin-left: 100px;
                display: flex;
                align-items: center;
                opacity: 0.5;
                transition: all .3s;
                &.active {
                    opacity: 1;
                    transition: all .3s
                }
                &:after {
                    content: "";
                    background: url(${ArrowStep}) no-repeat center center;
                    width: 8px;
                    height: 16px;
                    position: absolute;
                    right: -50px;
                }
                &:last-child {
                    &:after {
                        display: none;
                    }
                }
                &:first-child {
                    margin-left: 0px;
                }
                span {
                    font-size: 14px;
                    line-height: 17px;
                    letter-spacing: -0.1px;
                    color: #FFFFFF;
                    margin-left: 9px;
                }
            }
        }
    }
    .ambiente {
        display: flex;
        align-items: center;
        .icone {
            width: 36px;
            height: 36px;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: rgba(255, 255, 255, 0.1);
            margin-right: 15px;
        }
        span {
            max-width: 65px;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            color: #FFFFFF;
        }
    }
    @media(max-width: 1200px) {
        height: auto;
        padding: 20px 0px;
        ${Container} {
            flex-direction: column;
            align-items: center;
        }
        .esq {
            flex-direction: column;
            align-items: center;
            margin-bottom: 30px;
            a {
                margin-bottom: 30px;
            }
            ul {
                margin: 0;
            }
        }
    }
    @media(max-width: 480px) {
        
        .esq {
            ul {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                grid-gap: 15px;
                li {
                    margin-left: 0;
                    flex-direction: column;
                    img {
                        margin-bottom: 10px;
                    }
                    span {
                        margin: 0;
                        text-align: center;
                        line-height: 19px;
                        max-width: 100px;
                    }
                    &:after {
                        width: 8px;
                        height: 16px;
                        position: absolute;
                        right: -10px;
                        top: 3px;
                    }
                }
            }
        }
    }
`;