import React, { Component } from 'react';

import { Link } from 'gatsby';

import { StyleHaderCadastro } from './style';

import { Container } from '../../../Style/global';

import Logo from '../../../images/logo.svg';
import Cadeado from '../../../images/cadeado.svg';
import IconeDadosEmpresa from '../../../images/icone-dados-empresa.svg';
import IconeInfoContato from '../../../images/icone-info-contato.svg';
import IconeIntegracao from '../../../images/icone-integracao.svg';

 
class HeaderCadastro extends Component {
    state = {  }
    render() { 
        return ( 
            <StyleHaderCadastro>
                <Container>
                    <div className="esq">
                        <Link to="/"><img src={Logo}/></Link>
                        <ul>
                            <li className="active">
                                <img src={IconeDadosEmpresa}/>
                                <span>Dados da Empresa</span>
                            </li>
                            <li>
                                <img src={IconeInfoContato}/>
                                <span>Informações de Contato</span>
                            </li>
                            <li>
                                <img src={IconeIntegracao}/>
                                <span>Integração</span>
                            </li>
                        </ul>
                    </div>
                    <div className="ambiente">
                        <div className="icone">
                            <img src={Cadeado}/>
                        </div>
                        <span>Ambiente seguro!</span>
                    </div>
                </Container>    
            </StyleHaderCadastro>
        );
    }
}
 
export default HeaderCadastro;