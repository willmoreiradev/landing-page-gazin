import React, { Component } from 'react'
import { StylePergunta } from './style';

import ArrowDown from '../../images/arrow-xs-down-blue.svg';

class ComponentPergunta extends Component {
    state = { 
        visible: false
    }
    render() { 
        return ( 
            <StylePergunta show_resposta={this.state.visible}>
                <div className="title-pergunta" onClick={() => this.setState({ visible : !this.state.visible })}>
                    <h3>{this.props.titulo_pergunta}</h3>
                    <img src={ArrowDown}/>
                </div>
                <p>{this.props.resposta}</p>
            </StylePergunta>
        );
    }
}
 
export default ComponentPergunta;