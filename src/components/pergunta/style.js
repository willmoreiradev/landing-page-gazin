import styled from 'styled-components';

export const StylePergunta = styled.div`
    overflow: hidden;
    margin-bottom: 32px;
    box-shadow: ${props => props.show_resposta ? '0px 13px 44px rgba(0, 0, 0, 0.11)' : 'none'};
    background-color: ${props => props.show_resposta ? '#FFFFFF' : 'rgba(235, 235, 240, 0.6)'};
    border-radius: 12px;
    width: 100%;
    height: ${props => props.show_resposta ? '235px' : '85px'};
    transition: all .3s;
    &:last-child {
        margin-bottom: 0px;
    }
    .title-pergunta {
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 85px;
        padding: 0px 38px;
        cursor: pointer;
        h3 {
            font: normal 500 14px/37px 'Montserrat';
            color: #000000;
        }
        img {
            transform: ${props => props.show_resposta ? 'rotate(180deg)' : 'rotate(0deg)'};
            transition: all .3s;
        }
    }
    p {
        padding-left: 38px;
        padding-right: 29px;
        color: #474B5B;
        font-size: 14px;
        line-height: 24px;
        display: ${props => props.show_resposta ? 'block' : 'none'}
    }
    @media(max-width: 480px) {
        height: ${props => props.show_resposta ? '235px' : '75px'};
        margin-bottom: 15px;
        .title-pergunta {
            padding: 0px 22px;
            height: 75px;
            h3 {
                font-size: 13px;
                line-height: 19px;
                max-width: 230px;
            }
        }
        p {
            padding: 0px 22px;
            font-size: 12px;
            line-height: 22px;
        }
    }
`;