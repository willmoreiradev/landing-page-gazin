import React, { Component } from 'react';
import { StyleSectionDuvidas } from './style';

import ComponentPergunta from '../pergunta';

import { Container } from '../../Style/global';

import IconeDuvidas from '../../images/icone-duvidas.svg';

class SDuvidas extends Component {
    state = {  }
    render() { 
        return ( 
            <StyleSectionDuvidas id="duvidas">
                <Container>
                    <div className="title" data-aos='fade-up'>
                        <img src={IconeDuvidas}/>
                        <h2>Perguntas Frequentes</h2>
                    </div>
                    <div className="all">
                        <div data-aos='fade-right'>
                            <ComponentPergunta
                                titulo_pergunta="O que é um Marketplace?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Quais serão as vantagens em utilizar o Marketplace?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Quanto vou pagar pela parceria com o nosso Marketplace?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Como receberei o pagamento pelas vendas efetuadas?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Sou MEI e emito nota fiscal, posso cadastrar minha loja?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                        </div>
                        <div data-aos='fade-left'>
                            <ComponentPergunta
                                titulo_pergunta="A emissão da nota fiscal dos pedidos é feita pela Gazin?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Como funciona o frete Marketplace da Gazin?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="A Gazin se responsabiliza pelas entregas dos pedidos?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Quais os requisitos para vender no Marketplace da Gazin?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                            <ComponentPergunta
                                titulo_pergunta="Não tenho loja virtual. Posso realizar o cadastro mesmo assim?"
                                resposta="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum"
                            />
                        </div>
                    </div>
                </Container>
            </StyleSectionDuvidas>
        );
    }
}
 
export default SDuvidas;