import styled from 'styled-components';

export const StyleSectionDuvidas = styled.section`
  padding-bottom: 212px;
  .title {
      display:flex;
      flex-direction: column;
      align-items: center;
      img {
          margin-bottom: 28px;
      }
      h2 {
        font: normal bold 36px/52px 'Montserrat';
        color: #363843;
      }
  }
  .all {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 32px;
    align-items: flex-start;
    margin-top: 60px;
  }
  @media(max-width: 1100px) {
    .all {
        grid-template-columns: 1fr;
        grid-gap: 38px;
    }
  }
  @media(max-width: 480px) {
    padding-bottom: 143px;
    img {
      margin-bottom: 5px;
      max-width: 72px;
    }
    .title {
      h2 {
        text-align: center;
        font-size: 28px;
        line-height: 52px;
      }
    }
    .all {
      grid-gap: 15px;
      margin-top: 25px;
    }
  }
`;