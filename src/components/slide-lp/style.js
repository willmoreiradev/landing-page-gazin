import styled from 'styled-components';

import BgAzul from '../../images/bg-lp.png';

export const SlideLp = styled.section`
    position: relative;
    padding-top: 193px;
    height: 826px;
    &:after {
        content: "";
        background: url(${BgAzul}) no-repeat center center;
        position: absolute;
        top: 0;
        right: 0;
        width: 781px;
        height: 826px;
    }
    @media(max-width: 1440px) {
        &:after {
            width: 600px;
            height: 636px;
            background-size: 100%;
        }
    }
    .container {
        position: relative;
    }
    button {
        position: absolute;
        bottom: 0;
        left: 15px;
        box-shadow: 0px 12px 32px rgba(81, 85, 104, 0.19);
        border-radius: 6px;
        width: 64px;
        height: 64px;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 5;
    }
    .slide-home {
        position: relative;
        .swiper-slide {
            display: flex;
            align-items: flex-start;
            opacity: 0;
            transition: all .3s;
            &.swiper-slide-active {
                opacity: 1;
                transition: all .3s;
            }
        }
        .swiper-pagination {
            text-align: left;
            width: auto;
            bottom: 120px;
        }

    .texto {
        margin-top: 60px;
        max-width: 285px;
        margin-right: 94px;
        h2 {
            font: normal normal 36px/50px 'Montserrat'; 
            color: #474B5B;
            margin-bottom: 28px;
            strong {
                font: normal 800 36px/50px 'Montserrat'; 
                color: #474B5B;
                display: block;
            }
        }
        p {
            line-height: 23px;
            color: #474B5B;
            margin-bottom: 33px;
            max-width: 288px;
        }
            a {
            background-color: #FFA82E;
            border-radius: 6px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 218px;
            height: 55px;
            padding-left: 29px;
            color: #FFFFFF;
            font-weight: bold;
            font-size: 14px;
            transform: scale(1);
            transition: all .3s;
            span {
                display: flex;
                align-items: center;
                justify-content: center;
                width: 58px;
                height: 100%;
                border-left: 1px solid rgba(255, 255, 255, 0.1);
            }
            &:hover {
                transform: scale(1.04);
                transition: all .3s;
            }
        }
    }
}
@media(max-width: 1600px) {
    padding-top: 140px;
    height: 645px;
    .slide-home {
        .texto {
            margin-top: 40px;
            h2 {
                font-size: 32px;
                line-height: 33px;
                margin-bottom: 20px
                strong {
                    font-size: 32px;
                    line-height: 39px;
                }
            }
            p {
                font-size: 14px;
                max-width: 260px;
                margin-bottom: 25px;
            }
        }
        .imagem-produto {
            width: 56%;
        }
        .swiper-pagination {
            bottom: 100px;
        }
    }
}
@media(max-width: 1100px) {
    height: auto;
    &:after {
        width: 900px;
        height: 950px;
        background-size: 100%;
    }
    .slide-home {
        .swiper-slide {
            width: 738px;
            flex-direction: column;
            align-items: center;
        }
        .texto {
            max-width: 100%;
            margin: 0;
            margin-bottom: 60px;
            h2 {
                text-align: center;
                color: #ffffff;
                strong {
                    color: #ffffff;
                }
            }
            p {
                margin: 0 auto;
                text-align: center;
                margin-bottom: 30px;
                max-width: 100%;
                color: #ffffff;
                font-size: 16px;
            }
            a {
                margin: 0 auto;
            }
        }
        .swiper-pagination {
            bottom: initial;
            top: 260px;
            width: 100%;
            text-align: center;
            .swiper-pagination-bullet {
                background-color: #ffffff;
                opacity: 0.4;
                &.swiper-pagination-bullet-active {

                    opacity: 1;
                }
            }
        }
        .imagem-produto {
            width: 89%;
        }
    }
    button {
        display: none;
    }
}
@media(max-width: 1050px) {
    padding-top: 100px;
    &:after {
        width: 550px;
        height: 580px;
        background-size: 100%;
        background-position-y: top;
        background-position-x: 110px;
    }
    .slide-home {
        .texto {
            h2 {
                font-size: 28px;
                margin-bottom: 21px;
                strong {
                    line-height: 44px;
                }
            }
            p {
                font-size: 14px;
                max-width: 262px;
                margin-bottom: 28px;
            }
        }
        .imagem-produto {
            width: 100%;
        }
        .swiper-pagination {
            top: 300px;
        }
    }
}
`;