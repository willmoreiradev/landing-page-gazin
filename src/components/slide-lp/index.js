import React, { useState } from 'react';
import Swiper from 'react-id-swiper';

import { SlideLp } from './style';
import { Container } from '../../Style/global';

import 'swiper/css/swiper.css';


import ArrowRight from '../../images/arrow-right-white.svg';
import Produto01 from '../../images/img-produto-01.png';
import ArrowDownBlue from '../../images/arrow-down-blue.svg';


const Pagination = () => {
    function clickSection(section) {
        const tabScroll = document.getElementById(section);
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': tabScroll.offsetTop - 80
        });   
    }
    const params = {
        speed: 600,
        pagination: {
        el: '.swiper-pagination',
            clickable: true
        }
    }
    return (
        <SlideLp id="home">
            <Container>
                <Swiper {...params} containerClass="slide-home">
                    <div>
                        <div className="texto" data-aos='fade-right'>
                            <h2>Um espaço <strong>para oferecer seus produtos</strong> </h2>
                            <p>Oferte seus produtos aos nossos clientes e decole suas vendas.</p>
                            <a href="" target="_blank">Eu quero <span><img src={ArrowRight}/></span></a>
                        </div>
                        <img src={Produto01} className="imagem-produto" data-aos='fade-left'/>
                    </div>
                    <div>
                        <div className="texto">
                            <h2>Um espaço <strong>para oferecer seus produtos</strong> </h2>
                            <p>Oferte seus produtos aos nossos clientes e decole suas vendas.</p>
                            <a href="" target="_blank">Eu quero <span><img src={ArrowRight}/></span></a>
                        </div>
                        <img src={Produto01} className="imagem-produto"/>
                    </div>
                </Swiper>
                <button data-aos='fade-up' type="button" 
                onClick={(e) => {
                    e.preventDefault();
                    clickSection('cadastro')
                }}>
                    <img src={ArrowDownBlue} className="imagem-produto"/>
                </button>
            </Container>
        </SlideLp>
    )
  };
  export default Pagination;